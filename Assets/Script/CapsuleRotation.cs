using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleRotation : MonoBehaviour
{
    [SerializeField]
    private Vector3 axes;
    public float rotationunits;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        axes=CapsuleMovement.ClampVector3(axes);
        transform.Rotate(rotationunits*(axes*Time.deltaTime));
    }
}
